"""
Abraham Tishelman-Charny
7 July 2022

The purpose of this script is to produce plot directories for ECAL DP notes to be uploaded to the ECAL approved plots website
"""

# imports 
import os 

# parameters 
fromPng = 0 # make directories and file transfers from files with png extensions 
fromDirecs = 1 # make directories and files based on existing directories 
onePlot = 0 # run over one plot as a test 
copyPlots = 0 # copy files from current directory to plot directories 
produceMetadata = 1 # copy template of metadata file to plot directory 

if(fromPng + fromDirecs != 1):
  raise Exception("Need to set either fromPng or fromDirecs to 1.")

if(fromPng): plotNames = [file.split('.')[0] for file in os.listdir(".") if file.endswith(".png")] # from png extensions
elif(fromDirecs): plotNames = [x[0].split('/')[-1] for x in os.walk(".") if x[0] != "."] # from existing directories 
print("plotNames:",plotNames)

plotNamesShort = [plotNames[0]]
PossibleExtensions = ["png", "pdf"]

Plot_MetaData_template = """
title: {title}
date: "2022-06-20" 
tags: ["EB", "TPG", "Trigger primitive generation", "trigger", "Double weights", "spikes"]
caption: {caption}
"""
if(onePlot):
    plotNamesToRun = plotNamesShort
else:
    plotNamesToRun = plotNames

for plotName in plotNamesToRun:
    print("plotName:",plotName)

    # copy plots
    if(copyPlots): 
        os.system("mkdir -p {plotName}".format(plotName = plotName))
        for Ext in PossibleExtensions:
            os.system("cp {plotName}.{Ext} {plotName}".format(plotName=plotName, Ext=Ext))

    # produce metadata file 
    if(produceMetadata):
        with open("{plotName}/metadata.yaml".format(plotName=plotName), "w") as metadataFile:
            metadata = Plot_MetaData_template
            metadataFile.write(metadata)
            metadataFile.close()
