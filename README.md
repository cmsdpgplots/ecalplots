# The repo is moved to https://gitlab.cern.ch/cms-rc/cmsdpgplots/ecalplots
# This repo is archived and will be deleted at some point.


# ECAL Plots

LSF ECAL plots storage

## LFS

First of all please read https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/

## Installing git LSF

on lxplus: it is **already installed**

on debian-like OS:

```
apt-get install git-lfs
```

on centos-like OS:

```
yum install git-lfs
```

## Cloning repository

Metadata files (`metadata.yaml`) are stored in the repository. For images and pdf files (and other binary files) Git LFS is used. It prevents from fetching large set of files during `git clone`.

Run:

```bash
git lfs install --skip-smudge
```

This will update your local ~/.gitconfig with LFS settings. 
`--skip-smudge` prevents cloning of LSF objects.


If you cant to fetch actual files use:

```bash
git lfs fetch --all
```

To fetch single file use

```bash
git lfs pull --exclude="" --include <path to file or filename>
```

For more documentation read https://github.com/git-lfs/git-lfs/blob/master/docs/man/git-lfs-fetch.1.ronn

## Editing files

Edit text files as usual you do it with git repository.

```bash
vim <file>
git add <file>
git commit <file>
git push
```

To edit and/or add binary files firstly set up LFS (see above) and checkout checkout them, then edit as usual file. It will be uploaded to LFS automatically (see `.gitattributes` file)
